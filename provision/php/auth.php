<?php

/**
 * Auto-loaded via auto_prepend_file php directive in pixi.lan vhost.
 *
 * Can be disabled in .htaccess per application/service with:
 * php_value auto_prepend_file none
 */

if (file_exists($authInclude = '/opt/pixi/pixi-sdk-login/__include.php')) {
    require_once $authInclude;
}
