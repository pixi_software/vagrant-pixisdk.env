#!/bin/bash

set -e
set -u

# $REV is used to force new provision-lock
# files
REV="20160801"
export DEBIAN_FRONTEND=noninteractive

LOG="/tmp/provision.log"

rm -f $LOG

function say() {
  echo " -> ${1}"
}

function _check_and_exec() {
  LOCK="/var/.provision-lock-${REV}-${1}";
  LOG="/var/.provision-lock-${REV}-${1}.log";

  if [ -f $LOCK ]; then
    say "Skipping $1 (lock file exists)"
  else
    say "Running $1 (> $LOG)"
    (eval _${1} | tee -a $LOG) && touch $LOCK
  fi;
}

# Main function
function _run() {
  say "Debug log: $LOG"
  _check_and_exec basic;
  _check_and_exec apt_sources;
  _check_and_exec apt_update;
  _check_and_exec apt_upgrade;
  _check_and_exec install_packages;
  _check_and_exec install_packages_php;
  _check_and_exec install_devtools;
  _check_and_exec install_devtools_roundcube;
  _check_and_exec install_devtools_pma;
  _check_and_exec composer_install;
  _check_and_exec npm_install;
  _check_and_exec configure_php;
  _check_and_exec configure_apache2;
  _check_and_exec configure_mysql;
  _services;
}

################################################################################
################################################################################
function _basic() {
  say "MOTD"
  echo "
  pixibox Vagrant built virtual machine, based
  on Ubuntu 14.04 LTS (trusty) x86_64
  " > /etc/motd.tail;
  rm -f /etc/update-motd.d/00-header /etc/update-motd.d/10-help-text
  sed -i '/ pam_motd.so /s/^/#/' /etc/pam.d/sshd

  say "Network plumbing"
  echo "pixibox" > /etc/hostname
  echo "127.0.0.1 localhost pixibox tools.pixi.lan pixi.lan" > /etc/hosts
  /bin/hostname -F /etc/hostname

  say "Setting timezone"
  echo "Europe/Ljubljana" > /etc/timezone

  say "Setting UTF8 locales"
  (locale-gen --purge en_US sl_SI.UTF-8 en_US.UTF-8 && update-locale) &>> $LOG
  export LANG="sl_SI.UTF-8"

  say "Disable unneeded services"
  /usr/sbin/update-rc.d -f chef-client remove
  /usr/sbin/update-rc.d -f puppet remove
}

################################################################################
################################################################################
function _apt_sources() {
  APT_CC='si'
  APT_VER='trusty'
  say "APT Sources"
  echo "# PHP 5.6.x, Apache 2.4.x
deb http://ppa.launchpad.net/ondrej/php/ubuntu ${APT_VER} main
deb-src http://ppa.launchpad.net/ondrej/php/ubuntu ${APT_VER} main
" >> /etc/apt/sources.list.d/pixi-apache-php.list

  say "Adding APT sign keys"
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E5267A6C &> /dev/null

  say "Adding NodeJS repo"
  curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash - &>> $LOG
}

################################################################################
################################################################################
function _apt_update() {
  say "Updating APT sources"
  apt-get update --fix-missing --yes &>> $LOG
  echo "";
}

################################################################################
################################################################################
function _apt_upgrade() {
  say "Upgrading packages (might take a while)"
  apt-get upgrade --yes &>> $LOG
  echo "";
}

################################################################################
################################################################################
function _install_packages() {
  say "Installing packages"

  apt-get install \
    python-software-properties \
    software-properties-common \
    build-essential \
    htop \
    mc \
    vim \
    curl \
    git-core \
    mercurial \
    nodejs \
    screen \
    memcached \
    mysql-server \
    apache2 \
    php5.6 \
    php5.6-curl \
    php5.6-dev \
    php5.6-gmp \
    php5.6-intl \
    php5.6-mbstring \
    php5.6-mcrypt \
    php5.6-mysql \
    php5.6-soap \
    php5.6-xml \
    duplicity \
    python-paramiko \
    --yes --force-yes 2>&1 | while read LINE; do echo $LINE &>> $LOG ; echo -n "."; done

  echo "";
}

################################################################################
# Need to install following PHP packages separately - dependency issue
################################################################################
function _install_packages_php() {
  say "Installing additional PHP packages"

  apt-get install \
    php5.6-imagick \
    php5.6-memcache \
    php5.6-xdebug \
    --yes --force-yes 2>&1 | while read LINE; do echo $LINE &>> $LOG ; echo -n "."; done

  echo "";
}

################################################################################
################################################################################
function _composer_install() {
  if [ ! -f /usr/local/bin/composer.phar ]; then
    say "Installing composer"

    curl -sS https://getcomposer.org/installer > /tmp/composer-install.php
    if [ $? -eq 0 ]; then
      php -f /tmp/composer-install.php -- --ansi --install-dir=/usr/local/bin &&
      ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
    fi;
  fi;
}

################################################################################
################################################################################
function _npm_install() {
  say "Installing global node modules bower, gulp, yeoman, phantomjs (might take a while)"
  sudo npm install -g gulp bower yo phantomjs 2>&1 | while read LINE; do echo $LINE &>> $LOG ; echo -n "."; done

  BASH_PROFILE="/home/vagrant/.bash_profile"

  if [ ! -e $BASH_PROFILE ]; then
    sudo -u vagrant touch $BASH_PROFILE
    sudo -u vagrant echo "export PHANTOMJS_BIN=/usr/bin/phantomjs" > $BASH_PROFILE
  else
    sudo -u vagrant echo "export PHANTOMJS_BIN=/usr/bin/phantomjs" >> $BASH_PROFILE
  fi;

  echo "";
}

################################################################################
################################################################################
function _install_devtools() {
  say "Setting up devtools basics"
  mkdir -p /opt/devtools
}

################################################################################
################################################################################
function _install_devtools_roundcube() {
  apt-get install --yes --force-yes mailutils php5.6-sqlite3 postfix courier-imap &>> $LOG

  VER="1.2.1"
  RC="roundcubemail-$VER"
  DST="/opt/devtools/roundcube"

  rm -rf $DST

  wget -q -O $RC \
    https://github.com/roundcube/roundcubemail/releases/download/$VER/$RC.tar.gz

  tar -zxf $RC -C /opt
  rm -f $RC
  mv /opt/$RC $DST

  DBD="/var/roundcube"
  mkdir -p $DBD
  chown www-data: $DST/logs $DST/temp $DBD

  echo "<?php

\$config['db_dsnw'] = 'sqlite:///${DBD}/sqlite.db?mode=0646';
\$config['default_host'] = 'localhost';
\$config['smtp_server'] = '';
\$config['smtp_port'] = 25;
\$config['smtp_user'] = '';
\$config['smtp_pass'] = '';
\$config['support_url'] = '';
\$config['des_key'] = 'rlyrandomstring123456789';

// List of active plugins (in plugins/ directory)
\$config['plugins'] = [
    'archive',
];

// skin name: folder from skins/
\$config['skin'] = 'larry';

" > ${DST}/config/config.inc.php

  say "Configuring Postfix for RoundCube"

  sudo postconf -e "home_mailbox = Maildir/"
  sudo postconf -e "mailbox_command = "
  sudo postconf -e "virtual_alias_maps = regexp:/etc/postfix/virtual"
  sudo postconf -e "virtual_alias_domains ="
  echo "/.*/  vagrant" > /etc/postfix/virtual
  postmap /etc/postfix/virtual
  sudo  /etc/init.d/postfix restart

  # Sending welcome mail with some info (this needs to be done, so that imap will let us login into roundcube)
  echo "To enable message deleting, go to Settings > Preferences > Special Folders and press Save button." | mail -s "README first" foo@pixi.lan
}

################################################################################
################################################################################
function _install_devtools_pma() {
  VER="4.6.3"
  PHPMYADMIN="phpMyAdmin-$VER-english"
  DST="/opt/devtools/pma"

  rm -rf $DST

  wget -q -O $PHPMYADMIN \
    https://files.phpmyadmin.net/phpMyAdmin/$VER/$PHPMYADMIN.tar.gz

  tar -zxf $PHPMYADMIN -C /opt
  rm -f $PHPMYADMIN
  mv /opt/$PHPMYADMIN $DST

  echo "<?php
\$cfg['blowfish_secret'] = 's9U3hg6';
\$i = 1;
\$cfg['Servers'][\$i]['auth_type'] = 'config';
\$cfg['Servers'][\$i]['host'] = 'localhost';
\$cfg['Servers'][\$i]['AllowNoPassword'] = true;
\$cfg['Servers'][\$i]['user'] = 'root';

" > $DST/config.inc.php
}

################################################################################
################################################################################
function _configure_php() {
  say "Configuring PHP"

  echo "
date.timezone = \"Europe/Ljubljana\"
display_errors = On
error_reporting = -1
html_errors = 1
" | tee /etc/php/5.6/apache2/conf.d/99-custom.ini

  phpdismod -s cli xdebug
}

################################################################################
################################################################################
function _configure_apache2() {
  say "Configuring Apache2 vhosts"
  if [ ! -e /etc/apache2/sites-enabled/pixi.lan ]; then
    ln -s /opt/provision/apache2/pixi.lan.conf /etc/apache2/sites-available/
  fi;

  a2dissite 000-default
  a2ensite pixi.lan
}

################################################################################
################################################################################
function _configure_mysql() {
  say "Configuring mysql"

  if [ ! -e /etc/mysql/conf.d/innodb.cnf ]; then
    cp -f /opt/provision/mysql/innodb.cnf /etc/mysql/conf.d/
  fi;

  for dbname in \
    session \
    api_master \
    apps_master;
  do
      if [ ! -d /var/lib/mysql/$dbname ] ; then
        say "Importing MySQL database ${dbname}";
        echo "CREATE DATABASE ${dbname} CHARACTER SET utf8 COLLATE utf8_unicode_ci;" | mysql -u root
        zcat /opt/provision/mysql/$dbname.sql.gz | mysql -u root -D $dbname
      fi;
  done
}

################################################################################
################################################################################
function _services() {
  say "Making sure everything is running";

  for service in \
    apache2 \
    mysql \
    memcached;
  do
    say "Starting ${service}";
    service $service restart &>> $LOG
  done
}

################################################################################
################################################################################

_run

exit 0
