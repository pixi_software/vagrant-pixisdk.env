# pixi.box

## pixi* local development enviroment with:
> * apache2, php, composer, mysql, memcache
> * nodejs, npm, gulp, bower, yeoman

## Install
This repo should be cloned next to your other pixi* related code,
so that contents of `../` folder are available on 
[pixi.lan](http://pixi.lan) domain.

##### Clone repo
```
hg clone https://bitbucket.org/pixi_software/vagrant-pixisdk.env
```

##### Install required tools `vagrant` and `virtualbox`:

> * [Download virtualbox](https://www.virtualbox.org/wiki/Downloads)
> * [Download vagrant](https://www.vagrantup.com/downloads.html)
> * Restart host
> * IMPORTANT: Enter BIOS and check that virtualization features (if any) are enabled


##### Update your local `hosts` file with
```
10.10.0.2 pixi.lan tools.pixi.lan
```

## Usage
Change dir to your `vagrant-pixisdk.env` clone. Use `vagrant` to manage your virtual machine (VM).

##### Setup and start VM
First run will also provision virtual machine.
```
vagrant up
```

##### Stop VM
```
vagrant halt
```

##### SSH to VM
```
vagrant ssh
```

##### Suspend and resume VM
You can suspend your VM at any time with `vagrant suspend` and resume it with `vagrant resume`.

## Development
All folders one level above cloned folder (`../`) are avalilable @ [pixi.lan](http://pixi.lan) and at `/opt/pixi` in VM.

Support tools (roundcube, pma) can be found @ [tools.pixi.lan](http://tools.pixi.lan)

### Login and app master API dependencies
To use login and master API functionalities locally,
you have to clone `pixi_software/pixi-sdk-login` and `pixi_software/app-master-api`.
